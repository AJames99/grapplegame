﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAnimatorCallback : MonoBehaviour {
	[SerializeField]
	RigidbodyController m_rbcPlayer;

	public void Jump() {
		//m_rbcPlayer.Jump(new Vector3(0.0f, 15.0f, 0.0f));
		m_rbcPlayer.Jump(new Vector3(0.0f, m_rbcPlayer.m_flJumpForce, 0.0f));
	}

	public void Shoot() {
		m_rbcPlayer.Shoot();
	}
}
