%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: SkeletonLegs
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Clavicle
    m_Weight: 0
  - m_Path: Clavicle.001
    m_Weight: 0
  - m_Path: CPU
    m_Weight: 0
  - m_Path: EyeUnit.001
    m_Weight: 0
  - m_Path: Femur.L
    m_Weight: 0
  - m_Path: Femur.R
    m_Weight: 0
  - m_Path: Hand
    m_Weight: 0
  - m_Path: Hand.001
    m_Weight: 0
  - m_Path: Heel.L
    m_Weight: 0
  - m_Path: Heel.R
    m_Weight: 0
  - m_Path: Humerus
    m_Weight: 0
  - m_Path: Humerus.001
    m_Weight: 0
  - m_Path: Jaw
    m_Weight: 0
  - m_Path: Knee.L
    m_Weight: 0
  - m_Path: Knee.R
    m_Weight: 0
  - m_Path: Lower Teeth
    m_Weight: 0
  - m_Path: Pelvis
    m_Weight: 0
  - m_Path: RopeAttachment
    m_Weight: 0
  - m_Path: Scapula
    m_Weight: 0
  - m_Path: Scapula.001
    m_Weight: 0
  - m_Path: Shin.L
    m_Weight: 0
  - m_Path: Shin.R
    m_Weight: 0
  - m_Path: SkeletonArmature
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot
    m_Weight: 1
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1
    m_Weight: 1
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Blade.L
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Blade.L/Blade.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Blade.R
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Blade.R/Blade.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Clavicle.L
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Clavicle.L/Clavicle.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Clavicle.R
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Clavicle.R/Clavicle.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Neck
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Neck/Head
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Neck/Head/Jaw
      1
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/Neck/Head/Jaw
      1/Jaw_end
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/UpperArm.L
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/UpperArm.L/ForeArm.L
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/UpperArm.L/ForeArm.L/ForeArm.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/UpperArm.R
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/UpperArm.R/ForeArm.R
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Pelvis 1/Spine.01/Spine.02/Spine.03/UpperArm.R/ForeArm.R/ForeArm.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/HipsRoot/Thigh.L
    m_Weight: 1
  - m_Path: SkeletonArmature/HipsRoot/Thigh.L/Shin.L 1
    m_Weight: 1
  - m_Path: SkeletonArmature/HipsRoot/Thigh.L/Shin.L 1/Shin.L_end
    m_Weight: 1
  - m_Path: SkeletonArmature/HipsRoot/Thigh.R
    m_Weight: 1
  - m_Path: SkeletonArmature/HipsRoot/Thigh.R/Shin.R 1
    m_Weight: 1
  - m_Path: SkeletonArmature/HipsRoot/Thigh.R/Shin.R 1/Shin.R_end
    m_Weight: 1
  - m_Path: SkeletonArmature/Root
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/Elbow.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/Elbow.L/Elbow.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/Elbow.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/Elbow.R/Elbow.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Index01.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Index01.L/Index02.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Index01.L/Index02.L/Index03.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Index01.L/Index02.L/Index03.L/Index03.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Middle01.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Middle01.L/Middle02.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Middle01.L/Middle02.L/Middle03.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Middle01.L/Middle02.L/Middle03.L/Middle03.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Pinky01.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Pinky01.L/Pinky02.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Pinky01.L/Pinky02.L/Pinky03.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Pinky01.L/Pinky02.L/Pinky03.L/Pinky03.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Ring01.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Ring01.L/Ring02.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Ring01.L/Ring02.L/Ring03.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Ring01.L/Ring02.L/Ring03.L/Ring03.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Thumb01.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Thumb01.L/Thumb02.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Thumb01.L/Thumb02.L/Thumb03.L
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.L/Hand.L/Thumb01.L/Thumb02.L/Thumb03.L/Thumb03.L_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Index01.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Index01.R/Index02.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Index01.R/Index02.R/Index03.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Index01.R/Index02.R/Index03.R/Index03.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Middle01.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Middle01.R/Middle02.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Middle01.R/Middle02.R/Middle03.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Middle01.R/Middle02.R/Middle03.R/Middle03.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Pinky01.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Pinky01.R/Pinky02.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Pinky01.R/Pinky02.R/Pinky03.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Pinky01.R/Pinky02.R/Pinky03.R/Pinky03.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Ring01.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Ring01.R/Ring02.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Ring01.R/Ring02.R/Ring03.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Ring01.R/Ring02.R/Ring03.R/Ring03.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Thumb01.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Thumb01.R/Thumb02.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Thumb01.R/Thumb02.R/Thumb03.R
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/HandIK.R/Hand.R/Thumb01.R/Thumb02.R/Thumb03.R/Thumb03.R_end
    m_Weight: 0
  - m_Path: SkeletonArmature/Root/Knee.L 1
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/Knee.L 1/Knee.L_end
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/Knee.R 1
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/Knee.R 1/Knee.R_end
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/LegIK.L
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/LegIK.L/Foot.L
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/LegIK.L/Foot.L/Toes.L 1
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/LegIK.L/Foot.L/Toes.L 1/Toes.L_end
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/LegIK.R
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/LegIK.R/Foot.R
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/LegIK.R/Foot.R/Toes.R 1
    m_Weight: 1
  - m_Path: SkeletonArmature/Root/LegIK.R/Foot.R/Toes.R 1/Toes.R_end
    m_Weight: 1
  - m_Path: Skull
    m_Weight: 0
  - m_Path: Sternum
    m_Weight: 0
  - m_Path: Toes.L
    m_Weight: 0
  - m_Path: Toes.R
    m_Weight: 0
  - m_Path: Torso
    m_Weight: 0
  - m_Path: Ulna
    m_Weight: 0
  - m_Path: Ulna.001
    m_Weight: 0
  - m_Path: Upper Teeth
    m_Weight: 0
  - m_Path: Wrist
    m_Weight: 0
  - m_Path: Wrist.001
    m_Weight: 0
