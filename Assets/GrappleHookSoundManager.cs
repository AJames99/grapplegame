﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHookSoundManager : SoundManager {
	private const string deployString = "deploy";
	private const string contractString = "contract";
	private const string successString = "success";

	[SerializeField]
	private AudioClip deployClip;

	[SerializeField]
	private AudioClip contractClip;

	[SerializeField]
	private AudioClip sucessClip;

	private void Start() {
		base.Start();
		// Add the shootclips
		//clips.Add(deployString, deployClip);
	}

	public void PlayArmClip(bool isIn) {
		PlaySoundExplicit(isIn ? contractClip : deployClip, PlayMode.OneShot, 1.0f);
	}

	public void PlaySucessClip() {
		PlaySoundExplicit(sucessClip, PlayMode.OneShot, 1.0f);
	}
}
