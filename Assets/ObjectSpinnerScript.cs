﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpinnerScript : MonoBehaviour {

	Rigidbody rb;

	[SerializeField]
	private Vector3 spinSpeed;

	// Start is called before the first frame update
	void Start() {
		rb = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void FixedUpdate() {
		Quaternion rot = rb.rotation;
		rot *= Quaternion.Euler(spinSpeed * Time.fixedDeltaTime);
		rb.MoveRotation(rot);
	}
}
