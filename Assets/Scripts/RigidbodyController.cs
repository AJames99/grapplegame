﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public class RigidbodyController : MonoBehaviour {

	[Header("Movement")]

	[SerializeField]
	private float m_flMoveSpeed = 5.0f;
	private Vector2 m_v2MoveInput = Vector2.zero;
	[SerializeField]
	private float m_flMoveAccelAir = 2.0f;

	[SerializeField]
	private float m_flMaxVelocityChange = 10.0f;

	[SerializeField]
	private float m_flMouseSensitivity = 3.0f;
	private Vector2 m_v2MouseDelta = Vector2.zero;
	private float m_flPitch = 0.0f;
	private float m_flGunPitch = 0.0f;
	private float m_flYaw = 0.0f;
	private float m_flYawLerped = 0.0f;

	[SerializeField]
	private Transform m_tCameraParent;
	[SerializeField]
	private Transform m_tCameraCentre;
	[SerializeField]
	private Transform m_tCamera;

	// How much force should be applied per 1 unit of slant?
	//[SerializeField]
	//private float m_flRightingForcePerUnit;

	[SerializeField]
	public float m_flJumpForce = 25.0f;

	[SerializeField]
	private float m_flGravityForce = 5.0f;

	[SerializeField]
	private float m_flNormalMass = 8.0f;

	// An increased mass for when we're grounded (to allow for better control when moving on the ground vs rope)
	[SerializeField]
	private float m_flGroundedMass = 18.0f;

	[SerializeField]
	private float m_flGroundingDistance = 0.5f;

	[SerializeField]
	private LayerMask m_nGroundingIncludeLayers;
	[SerializeField]
	private LayerMask m_nCameraObscureLayers;
	[SerializeField]
	[Range(0.5f, 15.0f)]
	private float m_flCameraTargetDistance;

	[SerializeField]
	private float m_flStandingFriction = 5.0f;
	[SerializeField]
	private float m_flWalkingFriction = 0.1f;

	private int m_iReelDir;
	private bool m_bWantsToJump;
	private bool m_bCanJump;
	private const float JUMP_MIN_TIME = 0.5f;
	private float m_flNextJump = 0.0f;
	private bool m_bIsGrappleLoaded;
	private bool m_bIsGrappleLocked;
	private bool m_bIsGrounded;
	private bool m_bSnapToYaw = false;

	[Header("Grapple Hook")]
	[SerializeField]
	private GrappleHookScript m_ghGrappleHook;
	[SerializeField]
	private RopeSpawn m_rsRopeScript;
	[SerializeField]
	private Transform m_tGunLoadPoint;
	[SerializeField]
	private CharacterJoint m_cjGunLoadJoint;
	[SerializeField]
	private float m_flGrappleShotForce;
	[SerializeField]
	private float m_flGrappleShotDuration;
	[SerializeField]
	private float m_flRopeSprayRateStart = 15.0f;
	[SerializeField]
	private float m_flRopeSprayDuration = 2.5f;
	private float m_flRopeSprayTimer;
	private float m_flGrappleShotTimer;
	private bool m_bRopeSpraying;
	[SerializeField]
	private float m_flReelRate = 5.0f;
	[SerializeField]
	private Animator m_aniGrappler;

	private Rigidbody m_rbPlayer;
	private CapsuleCollider m_ccPlayerCollider;
	private Rigidbody m_rbGrappleHook;
	[SerializeField]
	private CharacterJoint m_cjCharJoint;
	//[SerializeField]
	//private ConfigurableJoint m_cjFauxSpring;
	[SerializeField]
	private float m_flSpringMassScaleGentle = 10.0f;
	[SerializeField]
	private float m_flSpringMassScaleHarsh = 0.2f;
	[SerializeField]
	private Transform m_tGunRotatePoint;
	[SerializeField]
	private float m_flGunRotationMultiplier = 1.25f;

	[Header("Sound/Audio")]
	[SerializeField]
	private GrappleGunSoundManager grappleSoundManager;

	[Header("Animation")]
	[SerializeField]
	private Animator m_anSkeletonAnimator;

	// Start is called before the first frame update
	void Start() {
		m_rbPlayer = GetComponent<Rigidbody>();
		m_ccPlayerCollider = GetComponent<CapsuleCollider>();

		m_rbPlayer.freezeRotation = true;
		//m_rbPlayer.useGravity = false;
		m_rbPlayer.useGravity = true;

		m_bCanJump = false;
		m_bIsGrounded = false;

		m_rbPlayer.mass = m_flNormalMass;

		m_rbGrappleHook = m_rsRopeScript.anchorObject.GetComponent<Rigidbody>();

		m_bIsGrappleLoaded = false;
		m_bIsGrappleLocked = false;
		m_bRopeSpraying = false;
		m_flRopeSprayTimer = 0.0f;

		//m_cjCharJoint = GetComponent<CharacterJoint>();
		//m_cjFauxSpring = GetComponent<ConfigurableJoint>();

		//m_cjFauxSpring.massScale = m_flSpringMassScaleHarsh;

		Cursor.visible = false;
	}

	// Update is called once per frame
	void Update() {
		CheckGrounded();

		Cursor.lockState = CursorLockMode.Locked; // Keep doing this. We don't want cursor anywhere just yet

		// Mouse-look
		if (m_v2MouseDelta.magnitude > 0) {
			//Vector3 cameraRot = m_tCameraParent.localRotation.eulerAngles;

			//cameraRot.y += m_v2MouseDelta.x * m_flMouseSensitivity;
			//cameraRot.x += m_v2MouseDelta.y * m_flMouseSensitivity;

			m_flPitch = Mathf.Clamp(m_flPitch + (-m_v2MouseDelta.y * m_flMouseSensitivity), -85.0f, 89.99f);

			//m_flYaw += (m_flPitch < 90f && m_flPitch > -90) ? m_v2MouseDelta.x * m_flMouseSensitivity : m_v2MouseDelta.x * -m_flMouseSensitivity; // for upside downsies
			m_flYaw += m_v2MouseDelta.x * m_flMouseSensitivity;
			//m_tCameraParent.localRotation = Quaternion.Euler(Vector3.right * m_flPitch);
			//m_rbPlayer.MoveRotation(Quaternion.Euler(Vector3.up * m_flYaw)); //moved this to lerping

			//Mathf.Clamp(cameraRot.x, 4.0f, 89.0f);
			//m_tCameraParent.rotation = Quaternion.Euler(cameraRot);

			// Rotate the gun around its rotation point
			// If pitch = 0, gun pitch = 0. If pitch = +45, gun 
			//m_tGunRotatePoint.parent.localRotation = Quaternion.Euler(Vector3.right * Mathf.Clamp(m_flPitch * m_flGunRotationMultiplier, -89, 35)); // removed for now, deferring to skeleton anim
		}

		if(m_v2MoveInput.magnitude > 0 || m_bSnapToYaw)
			LerpBodyRotation();

		// ==== Move the camera ====
		m_tCameraParent.localRotation = Quaternion.Euler(Vector3.right * m_flPitch + Vector3.up * m_flYaw); //Quaternion.Euler(Vector3.right * m_flPitch) * Quaternion.Euler(Vector3.up * m_flYaw);
		m_tCameraParent.position = m_tCameraCentre.position;

		float clampedDist = m_flCameraTargetDistance;
		RaycastHit hitInfo;
		// Compute camera visibility/occlusion
		if (Physics.Raycast(m_tCameraParent.position, -m_tCameraParent.forward, out hitInfo, m_flCameraTargetDistance, m_nCameraObscureLayers, QueryTriggerInteraction.Ignore)) {
			clampedDist = (m_tCameraParent.position - hitInfo.point).magnitude;
		}
		Vector3 updatedCameraPos = m_tCamera.localPosition;
		updatedCameraPos.z = -clampedDist + 0.15f;
		m_tCamera.localPosition = updatedCameraPos;

		Vector3 rootPosition = transform.position - (transform.up * m_ccPlayerCollider.height / 2);
		Vector3 topPosition = transform.position + (transform.up * m_ccPlayerCollider.height / 2);
		//Debug.DrawLine(rootPosition, topPosition, Color.red, 0.05f);

		// If the grapple is meant to be loaded, try to move it into the position before locking it in with a joint.
		if (m_bIsGrappleLoaded && !m_bIsGrappleLocked) {
			//m_rbGrappleHook.MovePosition(m_tGunLoadPoint.position);

			Quaternion desiredLoadedRotation = Quaternion.FromToRotation(Vector3.up, m_tGunLoadPoint.forward);
			//Quaternion desiredLoadedRotation = m_tGunLoadPoint.rotation;
			//m_rbGrappleHook.MoveRotation(desiredLoadedRotation);	// Force the rotation? (this appears to be causing the player-pull issue)
			m_rbGrappleHook.MoveRotation(Quaternion.Slerp(m_rbGrappleHook.rotation, desiredLoadedRotation, 20.0f * Time.deltaTime)); //Slerping seems to do the trick quickly without issues!


			// Translate via force (this appears to be causing the player-pull issue)
			Vector3 pullVec = (m_tGunLoadPoint.position - m_rbGrappleHook.position);
			pullVec *= 1 / Mathf.Max(pullVec.magnitude, 0.01f);
			//m_rbGrappleHook.AddForce(pullVec * 130.0f, ForceMode.Acceleration);  // This was a decent solution but it still was inconsistent and kinda slow
			m_ghGrappleHook.SetBeingPickedUp(true);                              // + adding drag was decent idea

			// <^> now just lerping to get rid of the force
			m_rbGrappleHook.MovePosition(Vector3.Lerp(m_rbGrappleHook.position, m_tGunLoadPoint.position, 20.0f * Time.deltaTime));

			m_rbGrappleHook.isKinematic = true; //test


			// Adjust spring to be gentler on the player if trying to load
			//m_cjFauxSpring.massScale = m_flSpringMassScaleGentle;

			// Rotate via torque
			//m_rbGrappleHook.AddTorque(m_tGunLoadPoint.up - m_rbGrappleHook.transform.up);

			//if ((m_rbGrappleHook.position - m_tGunLoadPoint.position).magnitude <= 0.05f && Vector3.Angle(m_rbGrappleHook.transform.up, m_tGunLoadPoint.forward) < 5) {
			if ((m_rbGrappleHook.position - m_tGunLoadPoint.position).magnitude <= 0.10f && Vector3.Angle(m_rbGrappleHook.transform.up, m_tGunLoadPoint.forward) < 0.5) {

				// Once the pull force has aligned it, we can safely forcibly move it a few cm (this isn't causing the pull issue)
				m_rbGrappleHook.MovePosition(m_tGunLoadPoint.position);
				m_rbGrappleHook.transform.position = m_tGunLoadPoint.position;
				m_rbGrappleHook.MoveRotation(desiredLoadedRotation);
				m_rbGrappleHook.transform.rotation = desiredLoadedRotation;

				// Attach the grapple body to the gun
				//m_cjGunLoadJoint.connectedBody = m_rbGrappleHook;               //// <^> TEMP
				
				// Eliminate the mass in the char joint we have connecting ourselves to the rope, to avoid the rope affecting us when it's loaded.
				//m_cjCharJoint.massScale = 0.0f;
				//m_cjCharJoint.connectedMassScale = 9999.0f;

				m_bIsGrappleLocked = true;

				//m_cjFauxSpring.massScale = m_flSpringMassScaleHarsh;

				m_ghGrappleHook.SetBeingPickedUp(false);

				//m_rbGrappleHook.isKinematic = false;			// <^> TEMP

				m_aniGrappler.SetTrigger("Load");
			}
		} else if (m_bIsGrappleLocked) {
			Quaternion desiredLoadedRotation = Quaternion.FromToRotation(Vector3.up, m_tGunLoadPoint.forward);
			m_rbGrappleHook.MoveRotation(desiredLoadedRotation);
			m_rbGrappleHook.MovePosition(m_tGunLoadPoint.position);
		}

		// Check jump reset timer
		if (!m_bCanJump) {
			m_bCanJump = Time.time >= m_flNextJump;
		}

		// Set the player friction
		m_ccPlayerCollider.material.dynamicFriction = (!m_bIsGrounded || m_v2MoveInput.magnitude > 0) ? m_flWalkingFriction : m_flStandingFriction;

		if (m_v2MoveInput.magnitude > 0) {

			// How fast are we meant to be moving according to our input?
			//			Vector3 targetVelocity = new Vector3(m_v2MoveInput.x, 0, m_v2MoveInput.y);
			//			targetVelocity = transform.TransformDirection(targetVelocity);
			//Vector3 targetVelocity = m_v2MoveInput.y < 0 ? -transform.forward : transform.forward; // not s = forward, s = back
			Vector3 targetVelocity = transform.forward;

			if (m_bIsGrounded) {
				//targetVelocity *= m_bIsGrounded ? m_flMoveSpeed : m_flMoveSpeedAir;
				targetVelocity *= m_flMoveSpeed;

				// Apply a force that attempts to reach our target velocity
				Vector3 velocity = m_rbPlayer.velocity;
				Vector3 velocityChange = targetVelocity - velocity;
				velocityChange.x = Mathf.Clamp(velocityChange.x, -m_flMaxVelocityChange, m_flMaxVelocityChange);
				velocityChange.z = Mathf.Clamp(velocityChange.z, -m_flMaxVelocityChange, m_flMaxVelocityChange);
				velocityChange.y = 0;
				m_rbPlayer.AddForce(velocityChange, ForceMode.VelocityChange);
			} else {
				targetVelocity *= m_flMoveAccelAir;
				m_rbPlayer.AddForce(targetVelocity, ForceMode.Acceleration);
			}
		}

		if (m_bIsGrounded) {
			// Jump
			if (m_bWantsToJump && m_bCanJump) {

				// moved to animation
				//m_rbPlayer.velocity = new Vector3(m_rbPlayer.velocity.x, m_flJumpForce, m_rbPlayer.velocity.z); // new jump (cancels vertical velocity

				m_bCanJump = false;
				m_flNextJump = Time.time + JUMP_MIN_TIME;

				m_anSkeletonAnimator.SetTrigger("Jump");
			}
		}

		// Handle rope extension after initiating firing
		if (m_bRopeSpraying) {
			if (Time.time <= m_flRopeSprayTimer) {

				// Diminish the reel out rate over time
				//float t = (m_flRopeSprayTimer - Time.time) / m_flRopeSprayDuration;
				//m_rsRopeScript.reelRate = Mathf.Lerp(0.0f, m_flRopeSprayRateStart, t);
				m_rsRopeScript.reelRate = m_flRopeSprayRateStart;

				// Reel out!
				m_rsRopeScript.ForceReel(1.0f);

				// Adjust spring to be gentler
				//m_cjFauxSpring.massScale = m_flSpringMassScaleGentle;

			} else {
				//FireGrapple(); // moved to somwhere seperate
				m_bRopeSpraying = false;
				m_rsRopeScript.reelRate = m_flReelRate;
				m_rsRopeScript.ForceReel(0);

				// Adjust spring to be harsher
				//m_cjFauxSpring.massScale = m_flSpringMassScaleHarsh;
			}
		}

		// Handle grapple firing (designed to occur slightly before the rope reel-out stops
		if (m_bIsGrappleLoaded && m_flGrappleShotTimer > 0) {
			if (Time.time >= m_flGrappleShotTimer) {
				FireGrapple();
			}
		}

		m_anSkeletonAnimator.SetBool("isGrounded", m_bIsGrounded);
		m_anSkeletonAnimator.SetFloat("moveSpeed", m_rbPlayer.velocity.ToHorizontal().magnitude * (m_v2MoveInput.magnitude > 0 ? 1 : 0));
		//m_anSkeletonAnimator.SetBool("walkingBackwards", m_v2MoveInput.y < 0);

		// Gravity Force (Manually done)
		//m_rbPlayer.AddForce(new Vector3(0, -m_flGravityForce * m_flNormalMass, 0), ForceMode.Force);
	}

	private void CheckGrounded() {
		Vector3 rootPosition = transform.position - (transform.up * m_ccPlayerCollider.height / 2);
		bool wasGroundedBefore = m_bIsGrounded;
		m_bIsGrounded = Physics.Raycast(rootPosition + new Vector3(0.0f, 0.05f, 0.0f), Vector3.down, m_flGroundingDistance, m_nGroundingIncludeLayers, QueryTriggerInteraction.Ignore);

		// If we've changed states, change mass
		if (wasGroundedBefore != m_bIsGrounded) {
			m_rbPlayer.mass = m_bIsGrounded ? m_flGroundedMass : m_flNormalMass;
			//Debug.Log("mass changed to " + m_rbPlayer.mass);

			//m_ccPlayerCollider.material.
		}

		Debug.DrawRay(rootPosition + new Vector3(0.0f, 0.05f, 0.0f), Vector3.down * m_flGroundingDistance, Color.red, 0.1f);
	}

	const float turnRate = 580.0f;
	private void LerpBodyRotation() {
		//m_v2MoveInput
		//m_rbPlayer
		//m_rbPlayer.MoveRotation(Quaternion.Euler(Vector3.up * m_flYaw));

		// For including the back run animation
		/*
				float yawMod = m_flYaw;
				if (m_v2MoveInput.magnitude > 0) {
					float angleDifference = Vector2.SignedAngle(m_v2MoveInput, new Vector2(0,1));

					if (Mathf.Abs(angleDifference) > 90) {
						angleDifference -= 180;
					} else if (Mathf.Abs(angleDifference) < -90) {
						angleDifference += 180;
					}

					yawMod += angleDifference.Clamp180();
				}
		*/


		float yawMod = m_flYaw;
		if (m_v2MoveInput.magnitude > 0) {
			//float angleDifference = Vector2.SignedAngle(m_v2MoveInput, new Vector2(0, 1));
			float angleDifference = Vector2.Angle(m_v2MoveInput, new Vector2(0, 1)).Clamp360();
			yawMod = Mathf.Atan2(Mathf.Sin(angleDifference), Mathf.Cos(angleDifference)); //small_angle=atan2(sin(angle),cos(angle))
			yawMod += angleDifference;//.Clamp360();
		}

		// If we're close enough, just snap 
		if (Mathf.Abs(yawMod - m_flYawLerped) <= turnRate * Time.deltaTime) {
			m_flYawLerped = yawMod;
		} else {
			m_flYawLerped += ((yawMod > m_flYawLerped) ? 1 : -1) * turnRate * Time.deltaTime;
		}

		//m_rbPlayer.MoveRotation(Quaternion.Euler(Vector3.up * m_flYawLerped));
		

		m_rbPlayer.MoveRotation(Quaternion.Slerp(m_rbPlayer.rotation, Quaternion.Euler(Vector3.up * (m_flYaw + Vector2.SignedAngle(m_v2MoveInput, new Vector2(0, 1)))), 15.0f * Time.deltaTime));

		if (m_bSnapToYaw) {
			m_bSnapToYaw = Quaternion.Angle(m_rbPlayer.rotation, Quaternion.Euler(Vector3.up * m_flYaw)) > 0.1f;
		}
	}

	private void LoadGrapple() {
		m_bIsGrappleLoaded = true;
		m_ghGrappleHook.SetGrappleActive(false);
		m_bIsGrappleLocked = false;

		m_rsRopeScript.SetRopeTangible(false);

		m_flGrappleShotTimer = 0;

		//m_rbGrappleHook.MovePosition(m_tGunLoadPoint.position);
		//m_rbGrappleHook.MoveRotation(m_tGunLoadPoint.rotation);

		//Quaternion upToForward = Quaternion.FromToRotation(m_rbGrappleHook.transform.up, m_tGunLoadPoint.forward);

		//m_cjGunLoadJoint.connectedBody = m_rbGrappleHook;
		//m_cjGunLoadJoint.anchor = Vector3.zero;
		//m_cjGunLoadJoint.autoConfigureConnectedAnchor = false;
		//m_cjGunLoadJoint.connectedAnchor = Vector3.zero;
	}

	public void OnLook(InputAction.CallbackContext context) {
		m_v2MouseDelta = context.ReadValue<Vector2>();
	}

	public void OnMove(InputAction.CallbackContext context) {
		m_v2MoveInput = context.ReadValue<Vector2>();
	}

	public void OnJump(InputAction.CallbackContext context) {
		m_bWantsToJump = context.ReadValueAsButton();
	}

	public void Jump(Vector3 force) {
		//m_rbPlayer.velocity = new Vector3(m_rbPlayer.velocity.x, 0.0f, m_rbPlayer.velocity.z) + force;
		m_rbPlayer.AddForce(force, ForceMode.VelocityChange);
	}

	public void OnShoot(InputAction.CallbackContext context) {
		if (m_bIsGrappleLoaded) {
			m_anSkeletonAnimator.SetFloat("aimPitch", m_flPitch);
			m_anSkeletonAnimator.SetBool("isShooting", context.ReadValueAsButton());
		}

		// Snap
		//m_rbPlayer.MoveRotation(Quaternion.Euler(Vector3.up * m_flYaw));
		m_bSnapToYaw = true;
	}

	// If The grapple is currently loaded, shoot out Rope fast (with diminishing rate) and almost immediately after fire the grapple.
	public void Shoot() {
		if (m_bIsGrappleLoaded) {
			//grappleSoundManager.PlayReelClip(false);

			//m_cjCharJoint.massScale = 1.0f;
			//m_cjCharJoint.massScale = 0.0f;
			//m_cjCharJoint.connectedMassScale = 9999.0f;

			m_bRopeSpraying = true;
			m_flRopeSprayTimer = Time.time + m_flRopeSprayDuration;
			m_flGrappleShotTimer = Time.time + (m_flGrappleShotDuration / 4);

			m_rsRopeScript.SetRopeTangible(true);
			m_ghGrappleHook.SetGrappleActive(true);

			// Try spring?
			SoftJointLimitSpring sp = m_cjCharJoint.swingLimitSpring;
			sp.spring = 500.0f;
			m_cjCharJoint.swingLimitSpring = sp;
			SoftJointLimitSpring sp2 = m_cjCharJoint.twistLimitSpring;
			sp2.spring = 500.0f;
			m_cjCharJoint.swingLimitSpring = sp2;

			// Animation
			m_aniGrappler.SetTrigger("Fire");
		}
	}

	private void FireGrapple() {

		grappleSoundManager.PlayShootClip();

		// Remove the grapple body from the gun
		m_cjGunLoadJoint.connectedBody = null;

		m_ghGrappleHook.FireGrapple(m_flGrappleShotForce, m_flGrappleShotDuration);

		m_bIsGrappleLocked = false;
		m_bIsGrappleLoaded = false;

		m_flGrappleShotTimer = 0;
		
		m_ghGrappleHook.SetBeingPickedUp(false);

		m_rbGrappleHook.isKinematic = false;
	}

	public void OnReel(InputAction.CallbackContext context) {

		// Don't allow any reeling if the grapple is currently loaded
		if (!m_bIsGrappleLoaded) {

			m_rsRopeScript.reelRate = m_flReelRate;
			// Get the rope script, reel us in
			m_rsRopeScript.OnReel(context);
			// If there's enough tension in the rope, give us an extra little bit of force in its direction so it doesn't break the physics as easily
			// If the rope is at its minimum, try to gently lerp its position into the holder object slot.

			// If the player is reeling and the grapple isn't dug into something, make the player much heavier in effect in the joint (reduces jitter)
			// otherwise, lighter
			/*float direction = context.ReadValue<float>();
			if (direction != 0.0f || m_ghGrappleHook.isGrappled) {
				// Adjust spring to be gentler on the player if reeling
				m_cjFauxSpring.massScale = m_flSpringMassScaleGentle;
			} else {
				// Adjust spring to be harsher on the player if we stop reeling
				m_cjFauxSpring.massScale = m_flSpringMassScaleHarsh;
			}*/

			if (m_rsRopeScript.isSafeToLoad && !m_ghGrappleHook.isGrappled) {
				LoadGrapple();
			}
		}
	}

	public void OnGrappleDeploy(InputAction.CallbackContext context) {
		if (m_ghGrappleHook) {
			m_ghGrappleHook.OnClaw(context);
		}
	}
}
