﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour {

	public float m_flVolume = 1.0f;

	AudioSource audioSource;

	protected Dictionary<string, AudioClip[]> clips = new Dictionary<string, AudioClip[]>();

	public enum PlayMode {
		Looped,
		OneShot,
		InWorld
	}

	// Start is called before the first frame update
	protected void Start() {
		audioSource = GetComponent<AudioSource>();
		if (!audioSource)
			Debug.LogError("No audio source found on SoundManager object!");

		audioSource.volume = m_flVolume;
		//Random.InitState(System.DateTime.Now.Millisecond);
	}

	public void PlaySound(string name, PlayMode playMode, float volumeScale = 1, Vector3 position = default(Vector3)) {
		AudioClip[] value;
		AudioClip clipToPlay;

		clips.TryGetValue(name, out value);
		if (value.Length > 1) {
			clipToPlay = value[value.GetRandomIndex()];
		} else {
			clipToPlay = value[0];
		}

		switch (playMode) {
			case PlayMode.Looped:
				audioSource.clip = clipToPlay;
				audioSource.loop = true;
				audioSource.Play();
				break;
			case PlayMode.InWorld:
				AudioSource.PlayClipAtPoint(clipToPlay, position, m_flVolume * volumeScale);
				break;
			case PlayMode.OneShot:
				audioSource.PlayOneShot(clipToPlay, volumeScale);
				break;
			default:
				break;
		}
	}
	public void PlaySoundExplicit(AudioClip clipToPlay, PlayMode playMode, float volumeScale, Vector3 position = default(Vector3)) {
		switch (playMode) {
			case PlayMode.Looped:
				audioSource.clip = clipToPlay;
				audioSource.loop = true;
				audioSource.Play();
				break;
			case PlayMode.InWorld:
				AudioSource.PlayClipAtPoint(clipToPlay, position, m_flVolume * volumeScale);
				break;
			case PlayMode.OneShot:
				audioSource.PlayOneShot(clipToPlay, volumeScale);
				break;
			default:
				break;
		}
	}

	public void Stop() {
		audioSource.Stop();
	}
}
