﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

// This script is to manage the little clumps of material that fall off when an object is grappled
public class DamageChunksScript : MonoBehaviour {

    //Rigidbody[] chunkBodies;
    Collider[] chunkColliders;
    bool[] isReleased;

    [SerializeField]
    float timeDecayMin = 0.5f;

    [SerializeField]
    float timeDecayMax = 5.0f;

    [SerializeField]
    float timeDestroy = 5.0f;

    bool isInitialised = false;

    public Material matToCopy;

    [SerializeField]
    private ParticleSystem poofSystem;
    private ParticleSystemRenderer poofDraw;
	private GameObject poofSystemOrphan;

    void Start() {
        UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);

		// |TODO| make the poof also occur on removal
        poofDraw = poofSystem.GetComponent<ParticleSystemRenderer>();

		// A copy
		poofSystemOrphan = Instantiate(poofSystem.gameObject);
		poofSystemOrphan.SetActive(false);

		if (!isInitialised)
            Init();
    }

    public void Init() {
        chunkColliders = transform.GetComponentsInChildren<Collider>();

        List<Collider> shuffledColliders = new List<Collider>();
        shuffledColliders.AddRange(chunkColliders);
        shuffledColliders.Shuffle();
        chunkColliders = shuffledColliders.ToArray();

        isReleased = new bool[chunkColliders.Length];

        Invoke("Decay", UnityEngine.Random.Range(timeDecayMin, timeDecayMax));

        if (matToCopy)
            MimicMaterial(matToCopy);
    }

    public void MimicMaterial(Material matToCopy) {
        foreach (Collider collider in chunkColliders) {
            MeshRenderer thisRenderer = collider.GetComponent<MeshRenderer>();
            thisRenderer.material = matToCopy;
        }

        // poof
        if (poofSystem && poofDraw) {
            poofDraw.material = matToCopy;
            poofSystem.Play();
        }
    }

    void Decay() {

        bool allReleased = true;

        for (int i = 0; i < isReleased.Length; i++) {

            if (!isReleased[i]) {
                //Debug.Log("Releasing rock.");
                chunkColliders[i].isTrigger = false;
                chunkColliders[i].attachedRigidbody.isKinematic = false;
                chunkColliders[i].attachedRigidbody.useGravity = true;
				chunkColliders[i].transform.parent = null;					// Wipe the parent so it is no longer stuck
				isReleased[i] = true;
                allReleased = false;
                break;
            }/* else {
                //Debug.Log("This was already released, next!");
                continue;
            }*/
        }
        if (!allReleased) {
            //Debug.Log("Gonna decay some more...");
            Invoke("Decay", UnityEngine.Random.Range(timeDecayMin, timeDecayMax));
        } else {
            Debug.Log("Gonna delete!");
            Invoke("Delete", timeDestroy);
        }
    }

	public void OnDetach() {
		// Make an orphan poof on detach
		poofSystemOrphan.SetActive(true);
		poofSystemOrphan.transform.parent = null;
		poofSystemOrphan.transform.position = transform.position;
		poofSystem = poofSystemOrphan.GetComponent<ParticleSystem>();
		poofDraw = poofSystem.GetComponent<ParticleSystemRenderer>();
		if (poofSystem && poofDraw) {
			poofDraw.material = matToCopy;
			poofSystem.Play();
		}
	}

    void Delete() {
		if (poofSystemOrphan)
			Destroy(poofSystemOrphan);

		foreach (Collider collider in chunkColliders) {
			Destroy(collider.gameObject);
		}
        Destroy(gameObject);
    }
}
