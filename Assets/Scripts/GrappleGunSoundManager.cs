﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleGunSoundManager : SoundManager {
	private const string shootString = "shoot";

	[SerializeField]
	private AudioClip[] shootClips;

	[SerializeField]
	private AudioClip reelInClip;

	[SerializeField]
	private AudioClip reelOutClip;

	private void Start() {
		base.Start();
		// Add the shootclips
		clips.Add(shootString, shootClips);
	}

	public void PlayShootClip() {
		PlaySound(shootString, PlayMode.OneShot, 0.5f);
	}

	public void PlayReelClip(bool isIn) {
		PlaySoundExplicit(isIn ? reelInClip : reelOutClip, PlayMode.OneShot, 1.0f);
	}
}
