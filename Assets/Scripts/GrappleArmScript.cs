using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GrappleArmScript : MonoBehaviour {

    FixedJoint fixedJoint;

    [SerializeField]
    float digForceMinimum = 2.5f;

    [SerializeField]
    float breakForce = 10.0f;

    [SerializeField]
    GameObject damageCluster;

    Collider thisCollider;
    Rigidbody thisRb;

    bool isTriggerCurrently;

    bool connectedRbIsFaux;
    Rigidbody connectedFauxRb;

    FixedJoint digJoint = null;

	DamageChunksScript lastChunkScript;

	[SerializeField]
	GrappleHookSoundManager m_ghsmGrappleSound;

	public bool canDig {
        set {
            _canDig = value;
        }
        get {
            return _canDig;
        }
    }
    bool _canDig = false;

    public bool isDug {
        /*set {
            _isDug = digJoint ? value : false;
        }*/
        get {
            return digJoint;
        }
    }
    private bool _isDug;

    // So that the grapple arm knows of its siblings and can detach if a joint is made / refuse joint creation if one is already present
    static GrappleArmScript[] grappleArms = new GrappleArmScript[0];

    // Start is called before the first frame update
    void Start() {
        thisRb = GetComponent<Rigidbody>();
        thisCollider = GetComponent<Collider>();

		isTriggerCurrently = false;
        thisCollider.isTrigger = false;

        digJoint = null;

        connectedRbIsFaux = false;

        canDig = false;

        if (grappleArms.Length <= 0) {
            grappleArms = transform.parent.GetComponentsInChildren<GrappleArmScript>();
        }
    }

    // Used only for checking the existence of the fixed joint for digging, to turn off the trigger state.
    private void FixedUpdate() {
        if (isTriggerCurrently) {
            if (!isDug) {
                thisCollider.isTrigger = false;
                isTriggerCurrently = false;

				//Debug.Log("Trigger status reverted.");
				
				if (lastChunkScript) {
					Debug.Log("Chunk script onDetach called");
					lastChunkScript.OnDetach();
				}
			}
        }
    }

    public void ForceJointBreak() {
        Destroy(digJoint);
        if (connectedFauxRb) {
            Destroy(connectedFauxRb.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision) {

        // Don't dig if we're disabled from digging.
        if (!canDig)
            return;

        // Only make a new one if the old one is gone (it should delete itself on break)
        if (!digJoint) {

            // Only one joint is allowed to be present at once, across all hook arms.
            foreach (GrappleArmScript sibling in grappleArms) {
                if (sibling.isDug) {
                    return;
                }
            }

			//if (collision.gameObject.isStatic && collision.gameObject.CompareTag("Grappleable")) {
			if (collision.gameObject.CompareTag("Grappleable")) {

				if ((collision.relativeVelocity.magnitude * thisRb.mass) > digForceMinimum) {
                    Rigidbody connectedBody = collision.rigidbody;
                    connectedRbIsFaux = false;

					// Only primitive colliders work with the rope with a rigidbody,
					// if using a mesh collider, we need to create a faux kinematic body to anchor the hook arm
					if (!collision.rigidbody) {

						// First delete the existing one
						if (connectedFauxRb) {
							Destroy(connectedFauxRb.gameObject);
						}

						GameObject fauxObject = new GameObject("Faux Hook Connection");
						fauxObject.transform.parent = collision.transform;//null;
						fauxObject.transform.position = collision.contacts[0].point;
						connectedBody = fauxObject.AddComponent<Rigidbody>();
						connectedBody.isKinematic = true;
						connectedBody.useGravity = false;
						connectedRbIsFaux = true;

						connectedFauxRb = connectedBody;

						// A fixed joint formed with a rigidbody with collision can be set to ignore collisions between the connected bodies.
						// This is great! But it doesn't work if the rigidbody and the collision are seperate, like we're doing here.
						// In this case, we elect to make the arm a trigger (temporarily) to ignore this. We check for the existence of the fixed joint
						// to disable this effect and return the object to a non-trigger state.
						thisCollider.isTrigger = true;
						isTriggerCurrently = true;
						//Debug.Log("Trigger status applied!");

						//Debug.Log("Creating faux rb...");
					} else if (!collision.gameObject.isStatic) { // If we have a rigidbody but it isn't static/kinematic...
																 // Let's add a separate body that is fixed to the parent body
						if (connectedFauxRb) {
							Destroy(connectedFauxRb.gameObject);
						}

						GameObject fauxObject = new GameObject("Faux Hook Connection (Moving)");
						fauxObject.transform.parent = null;
						fauxObject.transform.position = collision.contacts[0].point;
						connectedBody = fauxObject.AddComponent<Rigidbody>();
						connectedBody.isKinematic = false;
						connectedBody.useGravity = true;
						// Affix the new faux body to the rigidbody of the thing we hit
						FixedJoint fauxObjectFixed = fauxObject.AddComponent<FixedJoint>();
						fauxObjectFixed.connectedBody = collision.rigidbody;
						connectedRbIsFaux = true;

						connectedFauxRb = connectedBody;

						// A fixed joint formed with a rigidbody with collision can be set to ignore collisions between the connected bodies.
						// This is great! But it doesn't work if the rigidbody and the collision are seperate, like we're doing here.
						// In this case, we elect to make the arm a trigger (temporarily) to ignore this. We check for the existence of the fixed joint
						// to disable this effect and return the object to a non-trigger state.
						thisCollider.isTrigger = true;
						isTriggerCurrently = true;
						//Debug.Log("Trigger status applied!");

						//Debug.Log("Creating faux rb...");
					}

					// Play the associated sound
					m_ghsmGrappleSound.PlaySucessClip();

					digJoint = gameObject.AddComponent<FixedJoint>();
                    digJoint.anchor = collision.contacts[0].point;
                    digJoint.connectedBody = connectedBody;
                    digJoint.breakForce = breakForce;

                    // Visual clusters of debris
                    GameObject instance = Instantiate(damageCluster, collision.transform);
                    //Vector3 avg = Vector3.zero;
                    instance.transform.position = collision.contacts[0].point;
					instance.transform.localScale = instance.transform.localScale / instance.transform.lossyScale.magnitude;
					instance.transform.LookAt(collision.contacts[0].point + collision.contacts[0].normal);

					lastChunkScript = instance.GetComponent<DamageChunksScript>();
                    if (lastChunkScript) {
						MeshRenderer hitRenderer;
						// Confusing syntax? Try to get the renderer component into hitRenderer, but if that fails just get it in children.
						if (!collision.gameObject.TryGetComponent<MeshRenderer>(out hitRenderer)) {
							hitRenderer = collision.gameObject.GetComponentInChildren<MeshRenderer>();
						}

						if(hitRenderer) {
							lastChunkScript.matToCopy = hitRenderer.material;
						}

						lastChunkScript.Init();
                    }
                    //Debug.Log("Grapple arm dug into something!");
                    //isDug = true;
                }
            }
        }
    }
}
