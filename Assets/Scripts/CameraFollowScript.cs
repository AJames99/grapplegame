﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowScript : MonoBehaviour {

    [SerializeField]
    private Transform followObject;

    [SerializeField]
    [Range(30.0f, 100.0f)]
    private float FOVMin;

    [SerializeField]
    [Range(0.0f, 1000.0f)]
    private float distanceMin;

    [SerializeField]
    [Range(30.0f, 100.0f)]
    private float FOVMax;

    [SerializeField]
    [Range(0.0f, 1000.0f)]
    private float distanceMax;

    private Camera camera;

    // Start is called before the first frame update
    void Start() {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update() {
        camera.transform.LookAt(followObject, Vector3.up);

        float distance = (followObject.position - transform.position).magnitude;
        camera.fieldOfView = Mathf.Clamp(distance.Remap(distanceMin, distanceMax, FOVMax, FOVMin), FOVMin, FOVMax);
        //Debug.Log(camera.fieldOfView);
        //Debug.Log(distance);
    }
}
