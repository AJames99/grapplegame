﻿//using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods {
	public static Vector3 WithX(this Vector3 v, float x) {
		return new Vector3(x, v.y, v.z);
	}

	public static Vector3 WithY(this Vector3 v, float y) {
		return new Vector3(v.x, y, v.z);
	}

	public static Vector3 WithZ(this Vector3 v, float z) {
		return new Vector3(v.x, v.y, z);
	}

	public static Vector3 ToHorizontal(this Vector3 v) {
		return Vector3.ProjectOnPlane(v, Vector3.down);
	}

	public static float VerticalComponent(this Vector3 v) {
		return Vector3.Dot(v, Vector3.up);
	}

	public static Vector3 TransformDirectionHorizontal(this Transform t, Vector3 v) {
		return t.TransformDirection(v).ToHorizontal().normalized;
	}

	public static Vector3 InverseTransformDirectionHorizontal(this Transform t, Vector3 v) {
		return t.InverseTransformDirection(v).ToHorizontal().normalized;
	}

	public static float Remap(this float value, float from1, float to1, float from2, float to2) {
		return (((value - from1) / (to1 - from1)) * (to2 - from2)) + from2;
	}

	public static float RemapClamped(this float value, float from1, float to1, float from2, float to2) {
		return Mathf.Clamp((((value - from1) / (to1 - from1)) * (to2 - from2)) + from2, from2, to2);
	}

	public static float Clamp180(this float value) {
		while (value > 180 || value < -180) {
			value += value > 180 ? -360 : 360;
		}
		return value;
	}

	public static float Clamp360(this float value) {
		while (value > 360 || value < 0) {
			value += value > 360 ? -360 : 360;
		}
		return value;
	}

	public static void BroadcastAll(string fun, string tag, System.Object msg) {
		GameObject[] gos = GameObject.FindGameObjectsWithTag(tag);

		foreach (GameObject go in gos) {
			go.gameObject.BroadcastMessage(fun, msg, SendMessageOptions.DontRequireReceiver);

		}
	}

	public static int GetRandomIndex(this Object[] obj) {
		return Mathf.RoundToInt(Random.value * (obj.Length - 1));
	}

    public static void Shuffle<T>(this IList<T> list) {
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}


/*  Use this paradigm/syntax:
 *  
 *  private UserData _userData;
 *  public UserData MyUserData
 * {
 *  get => _userData;
 *  set => _userData = value ?? _userData;
 * }
 * 
 * 
 * */
