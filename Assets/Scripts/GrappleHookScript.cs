﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[System.Serializable]
public struct HookProperty {
	public float openVelocity;
	public float openForce;
    public float openDrag;
    public PhysicMaterial openMaterial;
    public float closedVelocity;
	public float closedForce;
    public float closedDrag;
    public PhysicMaterial closedMaterial;

	public float stemPickupDrag;
	public float stemDefaultDrag;

    public float shotForce;
}

public class GrappleHookScript : MonoBehaviour {

    [SerializeField]
    private Rigidbody[] hookArms;
    [SerializeField]
	private HingeJoint[] hookArmJoints;
    private MeshCollider[] hookArmColliders;
    private GrappleArmScript[] hookArmScripts;

	[SerializeField]
	private HookProperty hookProperty;

	[SerializeField]
	private Rigidbody m_rbStem;

	[SerializeField]
	private Collider m_cStem;
	[SerializeField]
	private float m_flStabilisationStrength = 10.0f;

	private int m_nDirection;
	private bool m_bClawOpenToggle;
	private bool m_bGrappleActive;
	private bool m_bThrusting;
	private float m_flThrustForce;
	private float m_flThrustTimer;

	[SerializeField]
	GrappleHookSoundManager m_ghsmGrappleSound;

	public bool isGrappled {
		get {
			foreach (GrappleArmScript arm in hookArmScripts) {
				if (arm.isDug)
					return true;
			}
			return false;
		}
	}

	// Start is called before the first frame update
	void Start() {
		m_nDirection = 0;
		m_bClawOpenToggle = false;
		m_bGrappleActive = true;
        hookArmColliders = new MeshCollider[hookArms.Length];
        hookArmScripts = new GrappleArmScript[hookArms.Length];
		m_bThrusting = false;
		m_flThrustTimer = 0.0f;
		m_flThrustForce = 0.0f;

		for (int i = 0; i < hookArms.Length; i++) {
            if (!hookArms[i].TryGetComponent<MeshCollider>(out hookArmColliders[i])) {
                Debug.LogError("COULD NOT GET COLLIDER @" + i);
            }
            if (!hookArms[i].TryGetComponent<GrappleArmScript>(out hookArmScripts[i])) {
                Debug.LogError("COULD NOT GET GRAPPLE ARM SCRIPT @" + i);
            }
        }

        ToggleClaw(m_bClawOpenToggle);
	}

	public void SetBeingPickedUp(bool pickup) {
		m_rbStem.drag = pickup ? hookProperty.stemPickupDrag : hookProperty.stemDefaultDrag;
		m_rbStem.angularDrag = pickup ? hookProperty.stemPickupDrag : hookProperty.stemDefaultDrag;
	}

	public void FireGrapple(float thrustForce, float thrustDuration) {
		m_bThrusting = true;
		m_flThrustTimer = Time.time + thrustDuration;
		m_flThrustForce = thrustForce;
	}

	public void SetGrappleActive(bool isActive) {
		for (int i = 0; i < hookArms.Length; i++) {
			Rigidbody rigidbody = hookArms[i];
			Collider collider = hookArmColliders[i];
			collider.enabled = isActive;
			// Only deployed/open arms should be allowed to dig into surfaces
			hookArmScripts[i].ForceJointBreak();
		}
		m_cStem.enabled = isActive;
	}

	private void FixedUpdate() {
		/*if (m_nDirection != 0) {
			m_rbStem.AddForce(m_rbStem.transform.up * hookProperty.shotForce * m_nDirection, ForceMode.Force);
		}*/
		if (m_bThrusting) {
			if (Time.time <= m_flThrustTimer) {
				m_rbStem.AddForce(m_rbStem.transform.up * m_flThrustForce);

				//m_rbStem.AddTorque(m_rbStem.transform.up * m_flStabilisationStrength, ForceMode.Acceleration);
				Vector3 difference = (m_rbStem.velocity.normalized - m_rbStem.transform.up);
				m_rbStem.AddForceAtPosition(difference * m_flStabilisationStrength, m_rbStem.transform.up, ForceMode.Acceleration);
			} else {
				m_bThrusting = false;
			}
		}
	}

	public void OnGrapple(InputAction.CallbackContext context) {
		//context.
		float direction = context.ReadValue<float>();
		m_nDirection = Mathf.RoundToInt(direction);

		m_rbStem.AddForce(m_rbStem.transform.up * hookProperty.shotForce * m_nDirection, ForceMode.Impulse);
	}

	public void OnClaw(InputAction.CallbackContext context) {
		if (m_bGrappleActive && !context.ReadValueAsButton()) { 
			m_bClawOpenToggle = !m_bClawOpenToggle;
			ToggleClaw(m_bClawOpenToggle);
		}
	}

	void ToggleClaw(bool bOpen) {
		m_ghsmGrappleSound.PlayArmClip(!bOpen);

		for (int i = 0; i < hookArms.Length; i++) {
            HingeJoint hinge = hookArmJoints[i];
            JointMotor motor = hinge.motor;         // struct
			motor.force = bOpen ? hookProperty.openForce : hookProperty.closedForce;
			motor.targetVelocity = bOpen ? hookProperty.openVelocity : hookProperty.closedVelocity;
			hinge.motor = motor;

            Rigidbody rigidbody = hookArms[i];
            Collider collider = hookArmColliders[i];
            collider.material = bOpen ? hookProperty.openMaterial : hookProperty.closedMaterial;
            rigidbody.drag = bOpen ? hookProperty.openDrag : hookProperty.closedDrag;

            // Only deployed/open arms should be allowed to dig into surfaces
            hookArmScripts[i].canDig = bOpen;
            if (!bOpen) {
                hookArmScripts[i].ForceJointBreak();
            }
        }
	}
}
