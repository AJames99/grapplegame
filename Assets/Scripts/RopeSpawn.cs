using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public class RopeSpawn : MonoBehaviour {

	[SerializeField]
	public GameObject partPrefab, parentObject, anchorObject;

	[SerializeField]
	Transform anchorPoint;

	[SerializeField]
	[Range(1, 150)]
	int length = 35;

	[SerializeField]
	[Range(3, 150)]
	int maxSegmentCount = 50;

	[SerializeField]
	[Range(1, 30)]
	int minSegmentCount = 3;

	[SerializeField]
	float partDistance = 0.21f;

	[SerializeField]
	bool reset, spawn, snapFirst, snapLast;

	private LineRenderer lineRenderer;
	[Header("Drawing")]
	[SerializeField]
	[Range(0.01f, 1.0f)]
	private float ropeDrawWidth;
	[SerializeField]
	[Range(-0.01f, 0.01f)]
	private float boneOffset = 0.00112f; // either 0.00112 or 0.001 is best
	private Vector3 basePosBottom = Vector3.zero;
	private Vector3 basePosTop = new Vector3(0.0f, 0.0f, 0.02f);

	// Drawing
	//MeshRenderer meshRenderer;
	SkinnedMeshRenderer skinnedMeshRenderer;

	MeshFilter filter;
	[SerializeField]
	Mesh segmentMeshReference;
	[SerializeField]
	Material testMaterial;
	Mesh mesh;
	Mesh clonedReferenceMesh;
	const int numVerticesPerLink = 4;
	//int triCount;
	int[] baseIndices = {
			0, 1, 4,
			1, 4, 5,
			0, 3, 7,
			0, 4, 7,
			3, 2, 6,
			3, 6, 7,
			1, 2, 5,
			2, 5, 6
		};
	Vector3[] ropeVertices;
	int[] ropeTriangles;
	Vector2[] ropeUVs;
	Vector3[] ropeNormals;

	private int segmentCount;
	[HideInInspector]
	public bool isSafeToLoad = false;

	Rigidbody anchorRB;

	[Header("Movement")]
	private Vector3 cranePosition;
	private Vector2 moveInput;

	[SerializeField]
	public  float reelRate = 2.0f;
	private float reelDirection;
	private float reelLerpAmount = 0.0f;

	[SerializeField]
	private float craneMoveSpeedVertical = 1.0f;
	[SerializeField]
	private float craneMoveSpeed = 5.0f;
	private int verticalMoveDir;
	[SerializeField]
	private float granularityFasterMultiplier = 2.0f;
	[SerializeField]
	private float granularitySlowerMultiplier = 0.25f;

	private int moveGranularity;

	[SerializeField]
	private Rigidbody rootObject;
	private CharacterJoint rootObjectJoint;
	private Vector3 rootObjectJointAnchorDefault;
	[SerializeField]
	private Transform rootObjectBindBone;

	[Header("Smart Projection Adjustment")]
	[SerializeField]
	private LayerMask excludedLayers;
	[SerializeField]
	private float projectionDistanceManyColliders = 0.2f;
	[SerializeField]
	private float projectionDistanceFewColliders = 0.01f;
	[SerializeField]
	private float highTension = 5.0f;
	[SerializeField]
	private float lowTension = 0.1f;

	// Keep track of the rope segments
	private List<Rigidbody> ropeRigidbodies;
	private List<CapsuleCollider> ropeColliders;
	private List<CharacterJoint> ropeJoints;
	private List<Transform> ropeBones;

	private void Start() {
		segmentCount = 0;

		lineRenderer = GetComponent<LineRenderer>();

		anchorRB = anchorObject.GetComponent<Rigidbody>();

		isSafeToLoad = false;

		Vector3 attachPointToRopeBind = transform.position - anchorPoint.position;
		Vector3 resultPosition = anchorRB.position + attachPointToRopeBind;
		anchorObject.transform.position = resultPosition;

		Spawn();

		ropeBones = new List<Transform>(segmentCount * 2);
		//int i = 0;
		foreach(Rigidbody ropeSeg in ropeRigidbodies) {
			SkinnedMeshRenderer smr = ropeSeg.GetComponentInChildren<SkinnedMeshRenderer>();
			ropeBones.Add(smr.bones[0]);      // top
			ropeBones.Add(smr.bones[1]);  // bottom
			//i += 2;
		}

		/*
		Vector3 scaling = new Vector3(ropeDrawWidth, 1.0f, ropeDrawWidth);
		Vector3 endOffset = new Vector3(0.0f, 0.0f, boneOffset);    // this is perfect for most
		bool boolIsTop = true;
		foreach (Transform bone in ropeBones) {
			bone.localPosition += boolIsTop ? endOffset : -endOffset;
			bone.localScale = scaling;
			boolIsTop = !boolIsTop;
		}*/

		Vector3 endOffset = new Vector3(0.0f, 0.0f, boneOffset);
		Vector3 scaling = new Vector3(ropeDrawWidth, 1.0f, ropeDrawWidth);
		bool boolIsTop = false;
		foreach (Transform bone in ropeBones) {
			//bone.localPosition = boolIsTop ? basePosTop + endOffset : basePosBottom - endOffset; //removed bottom positioning (always at 0 now)
			bone.localPosition = boolIsTop ? basePosTop + endOffset : basePosBottom;
			bone.localScale = scaling;
			boolIsTop = !boolIsTop;
		}

		// Min length must be less than max length. Else make it the same.
		minSegmentCount = minSegmentCount < maxSegmentCount ? minSegmentCount : maxSegmentCount;       

		rootObjectJoint = rootObject.GetComponent<CharacterJoint>();

		//rootObjectJointAnchorDefault = rootObjectJoint.anchor;
	}


	//const int maxColliders = 10;
	const int maxColliders = 2;
	const int minColliders = 0;
	private void FixedUpdate() {
		// Measure the net tensity across the rope
		float netTension = 0.0f;
		float[] tensions = new float[ropeJoints.Count];
		for (int i = 0; i < ropeJoints.Count; i++) {
			CharacterJoint joint = ropeJoints[i];
			tensions[i] = joint.currentForce.magnitude;
			netTension += joint.currentForce.magnitude;

			CapsuleCollider collider = ropeColliders[i];
			Rigidbody rigidbody = ropeRigidbodies[i];
			float overlaps = Physics.OverlapSphereNonAlloc(rigidbody.position, collider.height * 2.0f, null, ~excludedLayers, QueryTriggerInteraction.Ignore); // i hope null is ok here for the collider array
																																							   //            overlaps = Mathf.Clamp(overlaps, minColliders, maxColliders);

			// Increase the projection distance if we are colliding with more colliders
			float projectionDistanceNew = overlaps.RemapClamped(0.0f, 10.0f, projectionDistanceFewColliders, projectionDistanceManyColliders);

			// ...and increase it also based on the rope segment's tension
			//            projectionDistanceNew += tensions[i].RemapClamped(lowTension, highTension, projectionDistanceFewColliders, projectionDistanceManyColliders);
			//            projectionDistanceNew /= 2;

			joint.projectionDistance = projectionDistanceNew;
		}

		// For segments that are in contact with another body, and adjacent segments, increase their projection distance
	}

	// Update is called once per frame
	void Update() {
		if (reset) {
			foreach (Rigidbody rope in ropeRigidbodies) {
				Destroy(rope.gameObject);
			}

			reset = false;
		}

		if (spawn) {
			Spawn();
			spawn = false;
		}

		DrawRope();

		float moveSpeedMultiplier = 1.0f;
		if (moveGranularity > 0) {
			moveSpeedMultiplier = granularityFasterMultiplier;
		} else if (moveGranularity < 0) {
			moveSpeedMultiplier = granularitySlowerMultiplier;
		}

		// Old crane movement
		//cranePosition += new Vector3(moveInput.x * craneMoveSpeed, -verticalMoveDir * craneMoveSpeedVertical, moveInput.y * craneMoveSpeed) * Time.deltaTime * moveSpeedMultiplier;
		//rootObject.MovePosition(cranePosition);

		// Shift the base-most segment (closest to the root) in or out, extending the # of links as necessary
		reelLerpAmount += reelDirection * reelRate * Time.deltaTime;

		if (reelLerpAmount > 1) {
			segmentCount++;
			if (segmentCount == Mathf.Clamp(segmentCount, minSegmentCount, maxSegmentCount)) {
				reelLerpAmount -= 1;
				Extend();
			} else {
				segmentCount--;
				//Debug.Log("Maximum length reached! Stop trying to extend me :(");
			}
		} else if (reelLerpAmount < 0) {
			segmentCount--;
			if (segmentCount == Mathf.Clamp(segmentCount, minSegmentCount, maxSegmentCount)) {
				reelLerpAmount += 1;
				Dextend();
			} else {
				segmentCount++;
				isSafeToLoad = true;
				//Debug.Log("Minimum length reached! Stop trying to compress me :/");
			}
		}
		// Clamp to 0 to 1 range (only really necessary if we're trying to shorten but we already are at the minimum for example)
		reelLerpAmount = Mathf.Clamp01(reelLerpAmount);


		// really we need to move it in the back direction of the Nth rope segment
		// Move the root segment (potentially new root segment) OR its joint point to the described by the lerp amount relative to the root object
		//rootObjectJoint.anchor = new Vector3(0.0f, -reelLerpAmount * partDistance, 0.0f);   // Lower the connection point

		// in world pos first
		//Vector3 newAnchor = (rootObjectJoint.connectedBody.position - rootObjectJoint.connectedBody.transform.up * (partDistance * -reelLerpAmount));
		//newAnchor = rootObject.transform.InverseTransformPoint(newAnchor);// now local
		//rootObjectJoint.anchor = new Vector3(0.0f, -reelLerpAmount * partDistance, 0.0f) + rootObjectJointAnchorDefault;   // Lower the connection point
		rootObjectJoint.anchor = new Vector3(0.0f, -reelLerpAmount * partDistance, 0.0f) + rootObject.transform.InverseTransformPoint(rootObjectBindBone.position);  // added bone in!


		rootObjectJoint.autoConfigureConnectedAnchor = false;
		//rootObjectJoint.connectedAnchor = new Vector3(0.0f, -0.75f, 0.0f);
		rootObjectJoint.connectedAnchor = new Vector3(0.0f, -reelLerpAmount * partDistance, 0.0f);

		// let's force the rotation of the Nth segment
		Rigidbody lastRopeRB = ropeRigidbodies[ropeRigidbodies.Count - 1];
		lastRopeRB.MoveRotation(Quaternion.Euler(180, 0, 0));
		lastRopeRB.freezeRotation = true;
		lastRopeRB = ropeRigidbodies[ropeRigidbodies.Count - 2];
		lastRopeRB.MoveRotation(Quaternion.Euler(180, 0, 0));
		lastRopeRB.freezeRotation = true;
	}

	void Extend() {
		int lastIndexCurrent = ropeRigidbodies.Count - 1;

		// instance position = root rope position - partDistance;
		Rigidbody currentLast = ropeRigidbodies[lastIndexCurrent];
		CharacterJoint currentLastJoint = ropeJoints[lastIndexCurrent];

		currentLast.freezeRotation = false;
		ropeRigidbodies[lastIndexCurrent - 1].freezeRotation = false;

		currentLastJoint.anchor = new Vector3(0.0f, 1.0f, 0.0f); // reset its anchor

		Vector3 currentRootPos = ropeRigidbodies[lastIndexCurrent].position;
		Vector3 currentRootDir = Vector3.zero;//-ropeRigidbodies[lastIndexCurrent].transform.up;
		GameObject newSeg = Instantiate(partPrefab, currentRootPos - (currentRootDir * partDistance), Quaternion.identity, parentObject.transform);
		newSeg.name = "RopeSeg" + ropeRigidbodies.Count;

		newSeg.transform.eulerAngles = new Vector3(180, 0, 0); // is this needed?
		//newSeg.transform.LookAt(rootObject.position, Vector3.up);           // ##### This might be causing issues?  ######
		//newSeg.transform.rotation = currentLast.rotation;

		Rigidbody newLast = newSeg.GetComponent<Rigidbody>();
		CharacterJoint newLastJoint = newSeg.GetComponent<CharacterJoint>();

		//newLastJoint.connectedBody = ropeRigidbodies[lastIndexCurrent].GetComponent<Rigidbody>();
		newLastJoint.connectedBody = currentLast;
		newLastJoint.autoConfigureConnectedAnchor = false;
		newLastJoint.connectedAnchor = new Vector3(0.0f, -0.75f, 0.0f);

		ropeColliders.Add(newSeg.GetComponent<CapsuleCollider>());
		ropeRigidbodies.Add(newLast);
		ropeJoints.Add(newLastJoint);

		SkinnedMeshRenderer smr = newSeg.GetComponentInChildren<SkinnedMeshRenderer>();
		ropeBones.Add(smr.bones[0]);        // top
		ropeBones.Add(smr.bones[1]);        // bottom

		Vector3 scaling = new Vector3(ropeDrawWidth, 1.0f, ropeDrawWidth);
		//Vector3 endOffset = new Vector3(0.0f, 0.0f, 0.001f);
		Vector3 endOffset = new Vector3(0.0f, 0.0f, boneOffset);
		ropeBones[ropeBones.Count - 1].localPosition += endOffset; // top
		ropeBones[ropeBones.Count - 1].localScale = scaling;
		ropeBones[ropeBones.Count - 2].localPosition -= endOffset; // bottom
		ropeBones[ropeBones.Count - 2].localScale = scaling;

		// Attach the root to the new rope segment
		rootObjectJoint.connectedBody = newLast;

		//rootObjectJoint.massScale = 1000.0f;

		// Additionally, apply a counter-force to the root body to stop it from being pulled so jittery
		//rootObject.AddForce((rootObject.transform.TransformPoint(rootObjectJoint.anchor) - newLast.position) * 700.0f, ForceMode.Impulse);

		// Counterforce
		rootObject.AddForce(newLast.transform.up * 10.0f, ForceMode.Impulse); // was x15

		// Is it ok for the hook to be loaded into a gun at this point?
		isSafeToLoad = false;
	}

	void Dextend() {
		// We delete the last rope segment and replace it with the 2nd last (at the time).
		// The root object now connects to this new last.

		// The index of the current last rope segment (the one we're deletin')
		int lastIndexCurrent = ropeRigidbodies.Count - 1;
		// The body of the current last rope segment
		Rigidbody oldLast = ropeRigidbodies[lastIndexCurrent];

		// The body of the new last rope segment (2nd to last)
		Rigidbody newLast = ropeRigidbodies[lastIndexCurrent - 1];

		ropeColliders.RemoveAt(lastIndexCurrent);    // remove from colliders list
		ropeRigidbodies.RemoveAt(lastIndexCurrent);     // remove from the bodies list
		ropeJoints.RemoveAt(lastIndexCurrent);          // remove from joints list
		ropeBones.RemoveAt((2 * lastIndexCurrent) + 1); // remove top bone
		ropeBones.RemoveAt(2 * lastIndexCurrent);       // remove bottom bone
		
		// RIP old rope segment #34 
		Destroy(oldLast.gameObject);

		// Attach the root object to the new rope segment
		rootObjectJoint.connectedBody = newLast;

		// Counterforce
		rootObject.AddForce(-newLast.transform.up * 10.0f, ForceMode.Impulse); // was x15

		//rootObjectJoint.massScale = 100.0f;
	}

	public void Spawn() {
		segmentCount = (int)(length / partDistance);

		ropeColliders = new List<CapsuleCollider>(segmentCount);
		ropeJoints = new List<CharacterJoint>(segmentCount);
		ropeRigidbodies = new List<Rigidbody>(segmentCount);

		for (int i = 0; i < segmentCount; i++) {
			GameObject tmp;

			tmp = Instantiate(partPrefab, new Vector3(transform.position.x, transform.position.y + partDistance * (i + 1), transform.position.z), Quaternion.identity, parentObject.transform);
			tmp.transform.eulerAngles = new Vector3(180, 0, 0);

			//tmp.name = parentObject.transform.childCount.ToString();
			tmp.name = "RopeSeg" + i;

			//segmentColliders[i] = tmp.GetComponent<CapsuleCollider>();
			ropeColliders.Add(tmp.GetComponent<CapsuleCollider>());

			CharacterJoint thisJoint = tmp.GetComponent<CharacterJoint>();
			if (i == 0) {
				//Destroy(tmp.GetComponent<CharacterJoint>());
				thisJoint.connectedBody = anchorRB;
			} else {
				//tmp.GetComponent<CharacterJoint>().connectedBody = parentObject.transform.Find((parentObject.transform.childCount - 1).ToString()).GetComponent<Rigidbody>();
				thisJoint.connectedBody = ropeRigidbodies[i - 1].GetComponent<Rigidbody>();

				// test new adjustment
				thisJoint.autoConfigureConnectedAnchor = false;
				thisJoint.connectedAnchor = new Vector3(0.0f, -0.75f, 0.0f);
			}

			//ropeRigidbodies[i] = tmp.GetComponent<Rigidbody>();
			//ropeJoints[i] = tmp.GetComponent<CharacterJoint>();
			ropeRigidbodies.Add(tmp.GetComponent<Rigidbody>());
			ropeJoints.Add(thisJoint);
		}

		// Get the last one and make that our root bind
		cranePosition = ropeColliders[segmentCount - 1].GetComponent<Rigidbody>().position;
		rootObject.transform.position = cranePosition;
		CharacterJoint joint = rootObject.GetComponent<CharacterJoint>();
		joint.connectedBody = ropeColliders[segmentCount - 1].GetComponent<Rigidbody>();
		joint.enableCollision = false;
	}

	private void DrawRope() {
		// Almost seamless - roll/twist needs to be 1:1
		/*
		for (int i = 0; i < segmentCount; i++) {
			if (i > 0) {
				//ropeBones[(2 * i) + 1].rotation = Quaternion.Lerp(ropeRigidbodies[i - 1].transform.rotation, ropeRigidbodies[i].transform.rotation, .5f);
				ropeBones[(2 * i) + 1].rotation = Quaternion.Slerp(ropeRigidbodies[i - 1].transform.rotation, ropeRigidbodies[i].transform.rotation, .5f);
				ropeBones[(2 * i) + 1].rotation.Normalize();
			}
			if (i < segmentCount - 1) {
				//ropeBones[2 * i].rotation = Quaternion.Lerp(ropeRigidbodies[i + 1].transform.rotation, ropeRigidbodies[i].transform.rotation, .5f);
				//ropeBones[2 * i].rotation = Quaternion.Lerp(ropeRigidbodies[i].transform.rotation, ropeRigidbodies[i + 1].transform.rotation, .5f);
				ropeBones[2 * i].rotation = Quaternion.Slerp(ropeRigidbodies[i].transform.rotation, ropeRigidbodies[i + 1].transform.rotation, .5f);
				ropeBones[2 * i].rotation.Normalize();
			}

			// undo twist
//            Vector3 eulers = ropeBones[2 * i].localRotation.eulerAngles;
//           eulers.z = 0.0f;
//            ropeBones[2 * i].localRotation = Quaternion.Euler(eulers);
		}
		*/
		// new method - Top should copy the next "bottom" bone rotation, which is just the next rigidbody's rotation
		for (int i = 0; i < segmentCount; i++) {
			// Top bone rotation = next bottom bone rotation (world)
			//ropeBones[2 * i].rotation = ropeRigidbodies[i + 1].rotation;

			if (i > 0) {
				// Top bone rotation = previous bottom bone rotation (world)
				//ropeBones[(2 * i) + 1].rotation = ropeRigidbodies[i - 1].rotation; // inaccurate cause rb axes are different to bone's ?
				ropeBones[(2 * i) + 1].rotation = ropeBones[(2 * i) - 2].rotation;

				// Top bone position = previous bottom bone position (world)
				//ropeBones[(2 * i) + 1].position = ropeBones[(2 * i) - 2].position; // correct but without respect to offset
				ropeBones[(2 * i) + 1].position = ropeBones[(2 * i) - 2].position + (ropeBones[(2 * i) - 2].up * boneOffset); // correct with respect to offset!
			}
		}
	}

	public void OnVerticalMove(InputAction.CallbackContext context) {
		// F (down)
		if (context.ReadValue<float>() > 0) {
			verticalMoveDir = 1;
		}
		// R (up)
		else if (context.ReadValue<float>() < 0) {
			verticalMoveDir = -1;
		} else {
			verticalMoveDir = 0;
		}
	}
	public void OnCraneMove(InputAction.CallbackContext context) {
		moveInput = context.ReadValue<Vector2>();
	}

	public void OnGranularity(InputAction.CallbackContext context) {
		moveGranularity = Mathf.RoundToInt(context.ReadValue<float>());
	}

	public void OnReel(InputAction.CallbackContext context) {
		ForceReel(context.ReadValue<float>());
	}

	public void ForceReel(float direction) {
		reelDirection = direction;

		// Helps with reel jitter
		//rootObjectJoint.massScale = reelDirection == 0 ? 1.0f : 1000.0f;	// test 1
		//rootObjectJoint.massScale = reelDirection == 0 ? 1.0f : 100.0f;	// test 2

		//rootObjectJoint.massScale = reelDirection == 0 ? 1.0f : 50.0f;		// best?
		//rootObjectJoint.connectedMassScale = reelDirection == 0 ? 1.0f : 0.0001f;
	}

	public void SetRopeTangible(bool tangible) {
		/*foreach (CapsuleCollider cc in ropeColliders) {
			cc.enabled = tangible;
		}*/
		for (int i = 0; i < ropeColliders.Count; i++) {
			ropeColliders[i].enabled = tangible;
			//ropeJoints[i].connectedMassScale = tangible ? 1 : 0;
			//ropeJoints[i].massScale = tangible ? 1 : 9999;
		}
	}
}