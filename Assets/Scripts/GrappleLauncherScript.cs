using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleLauncherScript : MonoBehaviour {

	public Rigidbody m_rbGrappleAnchor;
	public Rigidbody m_rbGrappleRootSegment;
	public GameObject m_goSegmentVisual;

	public float m_flCapsuleRadius = 0.1f;
	public float m_flCapsuleHeight = 0.5f;
	public float m_flSegmentOffsetFactor = 0.8f;
	public float m_flSegmentMass = 0.5f;

	// Excluding root and anchor segments
	public int m_nDebugInitSegments = 10;

	private GameObject[] segments;


	const float SPRING_SPRING = 15000.0f;
	const float SPRING_DAMP = 55.0f;
	const float SPRING_MIN = 0.0f;
	const float SPRING_MAX = 0.005f;
	const float SPRING_TOLERANCE = 0.05f;

	// Start is called before the first frame update
	void Start() {
		if (m_rbGrappleAnchor && m_rbGrappleRootSegment) {
			segments = new GameObject[m_nDebugInitSegments];

			Vector3 currentPos = m_rbGrappleRootSegment.transform.position;
			Vector3 addDir = m_rbGrappleRootSegment.transform.up.normalized * (m_flCapsuleHeight * m_flSegmentOffsetFactor);

			for (int i = 0; i < m_nDebugInitSegments; i++) {
				currentPos += addDir;

				GameObject segment = new GameObject("CableSegment" + i);
				segment.transform.parent = m_rbGrappleRootSegment.transform.parent;
				segment.transform.position = currentPos;
				segment.transform.localRotation = m_rbGrappleRootSegment.transform.localRotation;
				segment.transform.localScale = m_rbGrappleRootSegment.transform.localScale;
				CapsuleCollider collider = segment.AddComponent<CapsuleCollider>();
				collider.center = Vector3.zero;
				collider.radius = m_flCapsuleRadius;
				collider.height = m_flCapsuleHeight;
				// y direction capsule

				Rigidbody rb = segment.AddComponent<Rigidbody>();
				rb.mass = m_flSegmentMass;

				SpringJoint spring = segment.AddComponent<SpringJoint>();
				spring.spring = SPRING_SPRING;
				spring.damper = SPRING_DAMP;
				spring.minDistance = SPRING_MIN;
				spring.maxDistance = SPRING_MAX;
				spring.tolerance = SPRING_TOLERANCE;

				spring.enablePreprocessing = false;


				if (i < 1)
					spring.connectedBody = m_rbGrappleRootSegment;
				else
					spring.connectedBody = segments[i - 1].GetComponent<Rigidbody>();

				spring.anchor = new Vector3(0.0f, -m_flCapsuleHeight / 2.0f, 0.0f);

				GameObject visual = Instantiate(m_goSegmentVisual);
				visual.transform.parent = segment.transform;
				visual.transform.localPosition = m_goSegmentVisual.transform.localPosition;
				visual.transform.localRotation = m_goSegmentVisual.transform.localRotation;
				visual.transform.localScale = m_goSegmentVisual.transform.localScale;

				segments[i] = segment;
			}

			SpringJoint anchorSpring;
			if (m_rbGrappleAnchor.TryGetComponent<SpringJoint>(out anchorSpring)) {
				anchorSpring.connectedBody = segments[m_nDebugInitSegments - 1].GetComponent<Rigidbody>();
				anchorSpring.autoConfigureConnectedAnchor = false;
				anchorSpring.connectedAnchor = new Vector3(0.0f, -0.0185f, 0.0f);
				anchorSpring.anchor = new Vector3(0.0f, -0.0185f, 0.0f);
				anchorSpring.spring = SPRING_SPRING;
				anchorSpring.damper = SPRING_DAMP;
				anchorSpring.minDistance = SPRING_MIN;
				anchorSpring.maxDistance = SPRING_MAX;
				anchorSpring.tolerance = SPRING_TOLERANCE;

				anchorSpring.enablePreprocessing = false;
			}

			//currentPos += addDir;
		}
	}

	// Update is called once per frame
	void Update() {

	}
}
