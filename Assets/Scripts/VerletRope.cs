using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public class VerletRope : MonoBehaviour {
	private LineRenderer lineRenderer;
	private List<RopeSegment> ropeSegments = new List<RopeSegment>();

    [SerializeField]
    private float ropeSegLength = 0.55f; //0.25

    [SerializeField]
    private int numberOfSegments = 35;

    [SerializeField]
    [Range(0.01f, 1.0f)]
    private float dampingMultiplier = 1.0f;

    [SerializeField]
    private float lineWidth = 0.1f;

    //[SerializeField]
    //private float orbitRadius = 1.5f;

    [SerializeField]
    private float reelRate = 0.5f;
    private int reelDir = 0;
    private float reelMin = 0.05f;
    private float reelMax = 1.0f;

    // To be controlled by WASD
    private Vector3 rootPosition;
    Vector2 moveInput;
    const float moveSpeed = 10.0f;

    [SerializeField]
    private LayerMask ropeCollisionExcludeLayers;

    private Vector3 ropeStartPointDefault = new Vector3(0.0f, 20.0f, 0.0f);

    [SerializeField]
    private GameObject testAnchor;
    private Rigidbody testAnchor_rb;
    private SphereCollider testAnchor_sc;
    [SerializeField]
    private Transform testAnchor_attachmentPoint;

    //private RaycastHit[] colliderOverlaps = new RaycastHit[10];

    private Collider[] overlappingColliders = new Collider[10];

    private CapsuleCollider[] capsuleColliders;

    const float sphereRadiusCollisionResolution = 10.0f;
    /*    [SerializeField]
        [Range(0.5f, 10.0f)]
        private float anchorRopeContractionVelMultiplier = 4.0f;

        [SerializeField]
        [Range(1.0f, 20.0f)]
        private float anchorRopeMaxVel = 15.0f;
    */

    // Start is called before the first frame update
    void Start() {
        rootPosition = ropeStartPointDefault;

        lineRenderer = GetComponent<LineRenderer>();
        //Vector3 ropeStartPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 ropeStartPoint = ropeStartPointDefault;
        //Vector3 ropeStartPoint = transform.position;

        capsuleColliders = new CapsuleCollider[numberOfSegments - 1];
        for (int i = 0; i < numberOfSegments - 1; i++) {
            GameObject ropeNode = new GameObject("RopeNode_" + i);
            capsuleColliders[i] = ropeNode.AddComponent<CapsuleCollider>();
            capsuleColliders[i].transform.position = ropeStartPoint - new Vector3(0.0f, ropeSegLength / 2, 0.0f);
            capsuleColliders[i].center = Vector3.zero;
            capsuleColliders[i].direction = 1; //y axis??
            capsuleColliders[i].radius = lineWidth * 2;
            capsuleColliders[i].height = ropeSegLength * 1.4f;
            capsuleColliders[i].isTrigger = true;

            ropeStartPoint.y -= ropeSegLength;
        }

        ropeStartPoint = ropeStartPointDefault;

        for (int i = 0; i < numberOfSegments; i++) {
			ropeSegments.Add(new RopeSegment(ropeStartPoint));
			ropeStartPoint.y -= ropeSegLength;
        }

        // testAnchor is bound to the last guy
        testAnchor_rb = testAnchor.GetComponent<Rigidbody>();
        testAnchor_sc = testAnchor.GetComponent<SphereCollider>();
        //testAnchor_rb.MovePosition(ropeStartPoint);
        ropeSegments[numberOfSegments - 1].connectedRigidbody = testAnchor_rb;

        Vector3 attachPointToRopeBind = ropeStartPoint - testAnchor_attachmentPoint.position;
        Vector3 resultPosition = testAnchor_rb.position + attachPointToRopeBind;
        testAnchor_rb.MovePosition(resultPosition);
    }

	// Update is called once per frame
	void Update() {
		DrawRope();
	}

    private void FixedUpdate()
    {
        if (reelDir != 0) {
            ropeSegLength += reelDir * reelRate * Time.fixedDeltaTime;
            ropeSegLength = Mathf.Clamp(ropeSegLength, reelMin, reelMax);
        }

        if (moveInput.magnitude > 0) {
            rootPosition += new Vector3(moveInput.x, 0, moveInput.y) * moveSpeed * Time.deltaTime;
        }

        Simulate();
    }


	private void Simulate() {
        // SIMULATION

        Vector3 forceGravity = new Vector3(0f, -1.5f, 0.0f);

        // CONSTRAINTS
        for (int i = 0; i < 50; i++)
        {
            ApplyConstraint();
        }

        // Update the collider positions
        for (int i = 0; i < ropeSegments.Count - 1; i++) {
            Vector3 centreNew = (ropeSegments[i].posNow + ropeSegments[i + 1].posNow) / 2;
            Vector3 upNew = ropeSegments[i].posNow - ropeSegments[i + 1].posNow;
            capsuleColliders[i].transform.position = centreNew;
            capsuleColliders[i].transform.rotation = Quaternion.FromToRotation(Vector3.up, upNew);
        }

        for (int i = 0; i < ropeSegments.Count; i++) {
            RopeSegment segment = ropeSegments[i];
			Vector3 velocity = segment.posNow - segment.posOld;
            segment.posOld = segment.posNow;
            //segment.posNow += velocity * dampingMultiplier;
            //segment.posNow += (forceGravity * Time.deltaTime) * dampingMultiplier;
            velocity += (forceGravity * Time.deltaTime);
            velocity *= dampingMultiplier;


            Vector3 displacementVel = velocity;// * Time.fixedDeltaTime;

            if (i < numberOfSegments - 1) {
                if (displacementVel.magnitude > capsuleColliders[i].radius) {
                    ClampDisplacement(ref velocity, ref displacementVel, segment.posNow);
                    capsuleColliders[i].transform.position += displacementVel;
                }

                Vector3 collisionDisplacement = ResolveCollisions(ref velocity, sphereRadiusCollisionResolution, capsuleColliders[i]);
                segment.posNow += collisionDisplacement;
            }
            segment.posNow += displacementVel;

            //segment.posNow += (velocity + displacement);

            if (segment.connectedRigidbody) {
                //segment.posNow += segment.connectedRigidbody.velocity;
                //Vector3 diff = segment.posNow - segment.connectedRigidbody.position;
                //segment.connectedRigidbody.MovePosition(segment.posNow);

                //Vector3 vectorRBToSegment = segment.posNow - segment.connectedRigidbody.position;
                //segment.connectedRigidbody.AddForce(vectorRBToSegment * 0.5f, ForceMode.VelocityChange);
                //segment.posNow -= vectorRBToSegment * 0.5f;

                Vector3 vectorRBToSegment = segment.posNow - testAnchor_attachmentPoint.position;
                //vectorRBToSegment = vectorRBToSegment.normalized * Mathf.Min(vectorRBToSegment.sqrMagnitude * anchorRopeContractionVelMultiplier, anchorRopeMaxVel); //sqr
                vectorRBToSegment = vectorRBToSegment.normalized * vectorRBToSegment.sqrMagnitude; //sqr

                segment.connectedRigidbody.AddForceAtPosition(vectorRBToSegment * 0.5f, testAnchor_attachmentPoint.position, ForceMode.VelocityChange);
                segment.posNow -= vectorRBToSegment * 0.5f;
            }

        }

        // CONSTRAINTS used to be here
    }

	private void ApplyConstraint() {
        // Constraint (First segment always follows the animation
        RopeSegment firstSegment = ropeSegments[0];
        //float cycledTime = Mathf.Sin(Time.time) + 1.0f;
        //firstSegment.posNow = new Vector3(Mathf.Cos(Time.time + cycledTime), Mathf.Sin(Time.time - cycledTime), Mathf.Sin(Time.time * 2 - cycledTime)) * orbitRadius + ropeStartPointDefault;
        firstSegment.posNow = rootPosition;

        for (int i = 0; i < numberOfSegments - 1; i++) {
            RopeSegment thisSeg = ropeSegments[i];
            RopeSegment nextSeg = ropeSegments[i + 1];

            float dist = (thisSeg.posNow - nextSeg.posNow).magnitude;
            float error = Mathf.Abs(dist - ropeSegLength);
            Vector3 changeDir = Vector3.zero;

            if (dist > ropeSegLength)
            {
                changeDir = (thisSeg.posNow - nextSeg.posNow).normalized;
            }
            else if (dist < ropeSegLength) {
                changeDir = (nextSeg.posNow - thisSeg.posNow).normalized;
            }

            Vector3 changeAmount = changeDir * error;

            /*if (i != 0)
            {
                thisSeg.posNow -= changeAmount * 0.5f;
                nextSeg.posNow += changeAmount * 0.5f;
            }
            else
            {
                nextSeg.posNow += changeAmount;
            }*/

            // Only do this step for i>0
            thisSeg.posNow -= (changeAmount * 0.5f * Mathf.Min(i, 1));
            // For i == 0 do += changeAmount, otherwise += changeAmount * 0.5f
            nextSeg.posNow += changeAmount * (0.5f + (0.5f * Mathf.Max(1 - i, 0)));
        }
    }

	private void DrawRope() {
		lineRenderer.startWidth = lineWidth;
		lineRenderer.endWidth = lineWidth;

		Vector3[] ropePositions = new Vector3[numberOfSegments];
		for (int i = 0; i < numberOfSegments; i++) {
			ropePositions[i] = ropeSegments[i].posNow;
		}

		lineRenderer.positionCount = ropePositions.Length;
		lineRenderer.SetPositions(ropePositions);
	}

	public class RopeSegment {
		public Vector3 posNow;
		public Vector3 posOld;
        public Rigidbody connectedRigidbody = null;
		public RopeSegment(Vector3 pos) {
			posNow = pos;
			posOld = pos;
		}
	}

    // Calculates the displacement required in order not to be in a world collider
    private Vector3 ResolveCollisions(ref Vector3 velocity, float sphereRadius, CapsuleCollider collider) {
        // Get nearby colliders
        Physics.OverlapSphereNonAlloc(transform.position, sphereRadius + 0.1f,
            overlappingColliders, ~ropeCollisionExcludeLayers, QueryTriggerInteraction.Ignore);

        var totalDisplacement = Vector3.zero;
        //var checkedColliderIndices = new HashSet<int>();

        // If the player is intersecting with that environment collider, separate them
        for (var i = 0; i < overlappingColliders.Length; i++)
        {
            // #### REMOVED ####
            // Two player colliders shouldn't resolve collision with the same environment collider
            /*if (checkedColliderIndices.Contains(i))
            {
                continue;
            }*/

            var envColl = overlappingColliders[i];

            // Skip empty slots
            if (envColl == null || envColl.isTrigger)
            {
                continue;
            }

            Vector3 collisionNormal;
            float collisionDistance;
            if (Physics.ComputePenetration(
                collider, collider.transform.position, collider.transform.rotation,
                envColl, envColl.transform.position, envColl.transform.rotation,
                out collisionNormal, out collisionDistance))
            {
                // Ignore very small penetrations
                // Required for standing still on slopes
                // ... still far from perfect though
                /*if (collisionDistance < _slopePenetrationIgnoreLimit) {
                    continue;
                }*/

                //checkedColliderIndices.Add(i);

                // Shift out of the collider
                totalDisplacement += collisionNormal * collisionDistance;

                // Clip down the velocity component which is in the direction of penetration
                velocity -= Vector3.Project(velocity, collisionNormal);
            }
        }

        // It's better to be in a clean state in the next resolve call
        for (var i = 0; i < overlappingColliders.Length; i++) {
            overlappingColliders[i] = null;
        }

        return totalDisplacement;
    }

    // If there's something between the current position and the next, clamp displacement
    private void ClampDisplacement(ref Vector3 velocity, ref Vector3 displacement, Vector3 position)
    {
        RaycastHit hit;
        if (Physics.Raycast(position, velocity.normalized, out hit, displacement.magnitude, ~ropeCollisionExcludeLayers))
        {
            displacement = hit.point - position;
        }
    }

    public void OnReel(InputAction.CallbackContext context) {
        // F (down)
        if (context.ReadValue<float>() > 0) {
            reelDir = 1;
        }
        // R (up)
        else if (context.ReadValue<float>() < 0) {
            reelDir = -1;
        }
        else {
            reelDir = 0;
        }
    }
    public void OnCraneMove(InputAction.CallbackContext context) {
        moveInput = context.ReadValue<Vector2>();
    }
}
